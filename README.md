# DevOps Application Assignment

This repo is made for candidates applying for a DevOps job in Kamstrup. 
We would like the candidate to engage in the assignment, to represent a personal solution in any upcoming interview. There is not a fixed solution for this assignment, so please engage with your own creativity and freedom of choice.

If you have any questions, please contact mhp@kamstrup.com


## Assignment description

We want you to design a "marketplace", where the frontend can display items to buy and previous purchases.
The focus for this assignment is to see that you know how to connect these services in a modern fashion and that you know how to write tests that can verify the functionality of your application.

The solution must have the following:

1. Frontend
  - provides the ability to buy new items
  - provides the ability to display previous purchases
  - can only contact the Backend (not the database)
2. Backend
  - provides an api accessible to the Frontend to get possible purchases stored in the database
  - provides an api accessible to the Frontend to get previous purchases stored in the database
  - can contact the database and be contacted by the frontend
3. "Off the shelf" database
  - fx MSSQL, MongoDB or PostgreSQL database 
  - can only be contacted by the backend

![](assignment-flow.png "Expected flow for the assignment")

In which languages and how you present the data is up to you, all that there is required is for us to be able to buy items in your marketplace and see what we've bought earlier on.
The frontend does not have to have a pretty UI, it can be a simple console application if you don't care for UI development.

Every application must be containerised in a __linux__ base and should have git repository, each project containing:

* Dockerfile (or build.sh if using buildah and podman)
* Source code (fx in src dir)
* Test code (fx in test dir)
* README.md explaining how to start, test and connect the application to the others as required

For those who are able to (don't use too much time on this if it is hard to setup for you) a pipeline that will build and test the application is a plus.

You can develop your application on any platform that is publicly available, but we are using gitlab.com in Kamstrup - getting familiar with it is an advantage for you if you are going to work at Kamstrup.  

If your project is private you must invite mhp@kamstrup.com.

__Key takeaway:__  Again, this is not about a flashy UI or goldplated solution. The important thing here are the *concepts* (not the *details*). Something that gives us a basis for kick-starting a solid conversation based on your solution :blush:


## Additions

The following is suggestions that you can choose to implement or not:

* Move your application to the cloud. What kinds of deployments might be benefitial to your solution in a given provider?
* Setup a docker-compose file
* Setup in kubernetes with scaleset
* Make sure that if a container crashes that it can startup again
* Emphasis on how the application is deployed
